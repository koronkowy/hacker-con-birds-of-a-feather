# hacker-con-birds-of-a-feather

This repository is mean to aggregate events for specific hacker conferences (also known as information security conferences) and group them by interest based on activity, identity, or experiences. This will hopefully enable people to find events.

Please see 2019 event information [here](https://gitlab.com/CircuitSwan/hacker-con-birds-of-a-feather/blob/master/hacker-summer-camp-blackhat-bsideslv-defcon-dianainitiative/2019/readme.md).

Request edit permissions to post a merge request by [following these directions](https://docs.gitlab.com/ee/user/project/members/#request-access-to-a-project).