# Buddy system

Sometimes it is great to explore a con with a group of people who will watch one anothers backs, or just are interested in new things.

If you want to participate sign up on the form - Join the Women and Non-Binary (WAN) squad discord to get access to the buddy sign up - contact @CircuitSwan

To work best please sign up, and look at the other people who ave signed up. Reach out to someone, or more than one someone. Talk before the event online. So a little OSINT. Look for flags. Feel free to buddy with other newbies!

What data you enter into the form should be public, keep OPSEC in mind

Remember to be cautious and meet in a public place, there is no vetting in place.

You’ll probably want to try and find people with the same interests so you can go to the same talks and villages and events.

Veterans are encouraged to find a buddy and help show them around and introduce them around.

First timer tips: https://sites.google.com/site/amazonv/first-conference 

The idea is to help you not be alone in a sea of people, and have someone to watch your back. Groups of >2 are even better.