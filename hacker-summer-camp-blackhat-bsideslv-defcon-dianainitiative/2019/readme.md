## Resources for 2019 Hacker Summer Camp

### What is Hacker Summer Camp?
It's the time period when many Information Security / Cyber Security / Hacker Conferences occur in Las Vegas, Nevada, USA

- https://www.blackhat.com/ 
- https://bsideslv.org
- https://defcon.org/
- https://www.queercon.org/ - open to all, and partnering with https://twitter.com/_beyondbinaries 
- https://www.dianainitiative.org/ - open to all, women focused
- https://narwhal.be/

### What is Birds of a Feather

We are trying to gather a list of as many resources, events and programs as possible occuring at summer camp and provide links to them to help you create the best time for you. We will try and tag the events with who the event or resource is aimed at, who is welcome, who is discouraged, so you can flock together with other birds like you.

### Chats / Discussion areas

- DEFCON27-squad - Women focused, non-binary welcome Discord ask @CircuitSwan
- [hackercon slack](https://join.slack.com/t/hackercon/shared_invite/enQtMjk0NTc1MjgzNjY1LWJlN2M1YmFjN2QzM2U4MzNlNTdiNTUyYmU4ZmU2Njc2YWVjY2E4OWIyZWMzZjQwYjRjYjUyMTg0YmQ0NzNiZTY), one Chanel per con, trying to keep the number of slacks lower!

### Social Media

- https://twitter.com/womenofdefcon - Women focused, non-binary welcome
- https://twitter.com/queercon - they will be having non-binary focused events with their partner
- https://twitter.com/defconparties - http://defconparties.com/ 
- https://twitter.com/_beyondbinaries
- https://twitter.com/wisporg
- https://twitter.com/WomenCyberjutsu
- https://twitter.com/CircuitSwan/lists/hacker-summer-camp

### Specific Events

#### *Buddy Program*

- Details:: [Buddy Program Read Me](https://gitlab.com/CircuitSwan/hacker-con-birds-of-a-feather/blob/master/hacker-summer-camp-blackhat-bsideslv-defcon-dianainitiative/2019/buddy-system.md)
- For: Newbies
- To sign up contact @CircuitSwan and get on the DEFCON27-squad discord

#### *DEF CON Meetup*

- Details: https://docs.google.com/document/d/1uw2Xhg89qR3RCHc4s1YhP59uKXXu-QZzOyhuK7TMZmI/edit
- Thursday August 8, 2019 5-7pm in Planet Hollywood’s Sin City Theatre
- For: For the women and non-binary folkx

### *DEF CON Shoot*

- https://deviating.net/firearms/defcon_shoot/
- Wednesday August 7, 2019
- For: everyone welcome

#### *Brunch*

- Details: RSVP https://www.meetup.com/Hacker-Foodies/events/262693310/?isFirstPublish=true
- Caesars Hotel Forum Food Court, 10am Sunday August 11, 2019
- For: the women and non-binary folkx

#### *Spa Day*

- Details: TDI Slack (The Diana Initiative)
- For: Women focused, non-binary welcome

#### *Photoshoot*

- Details: TBD
- For: Women focused, non-binary welcome

#### *Hacker Swan*

- Details: http://hackerswan.com
- For: everyone

#### *Hacker Foodies*

- Details: http://hackerfoodies.com
- For: everyone

#### *DEFCON Dinner*

- https://twitter.com/DEFCONDinner
- "Ok we have a go! Friday, August 9th, 2019 from 6:30 pm until 8:30 pm at the Park by @TMobile Arena. Plenty of dining & beverage choices. Come make friends, eat, imbibe & network before your Friday evening party schedule!"
- For: everyone

### Swag Planning

- Discussion in Discord (DEFCON27 squad) sign up below if you want to be ping'd when ordering capabilities are ready
- (Stickers)[https://rachelvelas.co/shop/]
- (Shirts)[https://wan-party.myshopify.com/collections/t-shirts] pigeon finished the shirt design for the WANsquad (women and non binary) - get yours now! (defcon27 label coming)

### Useful Applications

- https://hackertracker.app/ 

### Conference guides / tips-and-tricks

- https://sites.google.com/site/amazonv/first-conference?authuser=0

### Other Resource compliations (like this)